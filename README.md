# python-vlib-api

Simple API to perform api calls to the vlib stations details

# Usage

To get all stations data in one call use method getAllStations()

```python
from vlib_api import vlib_api
all_stations_data = vlib_api.getAllStations()
```

Will return a list of dictionaries with all data as follows

```json
[
    {
        "station": {
            "gps": {
                "latitude":48.85375581057431,
                "longitude":2.3390958085656166
            },
            "state":"Operative",
            "name":"André Mazet - Saint-André des Arts",
            "code":"6015",
            "type":"yes",
            "dueDate":1522965646
        },
        "nbBike":34,
        "nbEbike":8,
        "nbFreeDock":0,
        "nbFreeEDock":10,
        "creditCard":"yes",
        "nbDock":0,
        "nbEDock":55,
        "nbBikeOverflow":0,
        "nbEBikeOverflow":0,
        "kioskState":"yes",
        "overflow":"no",
        "overflowActivation":"no",
        "maxBikeOverflow":55,
        "densityLevel":1
    },
]
```

# Test

```bash
pip install -r requirements.txt
py.test
```

# Coverage

```bash
pip install -r requirements.txt
pip3 install coverage
coverage run --source=vlib_api/ -m py.test
coverage report
```

# Building package

```bash
python setup.py bdist_wheel
```