import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="vlib_api", # Replace with your own username
    version="0.0.1",
    author="Artiom FEDOROV",
    packages = ['vlib_api'],
    author_email="fedorov.artiom@gmail.com",
    description="Simple API to perform api calls to the station details",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://framagit.org/Akrobate/python-vlib-api",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'requests',
    ],
    python_requires='>=3',
)