''' Dummy test to check if pytest is ok '''
import requests

from vlib_api import vlib_api as vla

def test_searchStations(monkeypatch):

    def request_get(*args, **kwargs):
        class Response:
            def __init__(self):
                self.status_code = 200
                self.text = "{\"data\":1}"
        return Response()

    monkeypatch.setattr(requests, "get", request_get)

    response = vla.searchStations(
        48.83365042186074,
        2.4541277359451783,
        48.781028438836216,
        2.233456772344539,
        12.240790536767824
    )
    assert "data" in response
    assert response['data'] == 1


def test_getAllStations(monkeypatch):

    def request_get(*args, **kwargs):
        # Todo check params
        assert args[0] == 'https://www.velib-metropole.fr/webapi/map/details'
        assert kwargs == {'params': {'gpsTopLatitude': 48.99375585921062, 'gpsTopLongitude': 2.8059831283112375, 'gpsBotLongitude': 1.884475865134607, 'zoomLevel': 11, 'gpsBotLatitude': 48.68950533278223}}
        class Response:
            def __init__(self):
                self.status_code = 200
                self.text = "{\"data\":1}"
        return Response()

    monkeypatch.setattr(requests, "get", request_get)
    response = vla.getAllStations()
    assert "data" in response
    assert response['data'] == 1



def test_searchStations_bad_status_code(monkeypatch):

    def request_get(*args, **kwargs):
        class Response:
            def __init__(self):
                self.status_code = 404
                self.text = "{\"data\":1}"
        return Response()

    monkeypatch.setattr(requests, "get", request_get)

    try:
        vla.searchStations(
            48.83365042186074,
            2.4541277359451783,
            48.781028438836216,
            2.233456772344539,
            12.240790536767824
        )
    except Exception as inst:
        (message, status_code) = inst.args      # pylint: disable=unbalanced-tuple-unpacking
        assert type(inst) == Exception
        assert message == 'GET_REQUEST_BAD_STATUS'
        assert status_code == 404