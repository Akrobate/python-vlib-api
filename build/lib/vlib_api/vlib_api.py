import requests
import json

'''
query example
https://www.velib-metropole.fr/webapi/map/details
GET Params:
 - gpsTopLatitude=48.83365042186074
 - gpsTopLongitude=2.4541277359451783
 - gpsBotLatitude=48.781028438836216
 - gpsBotLongitude=2.233456772344539
 - zoomLevel=12.240790536767824
'''
def searchStations(top_latitude, top_longitude, bottom_latitude, bottom_longitude, zoom):
    get_params = {
        'gpsTopLatitude': top_latitude,
        'gpsTopLongitude': top_longitude,
        'gpsBotLatitude': bottom_latitude,
        'gpsBotLongitude': bottom_longitude,
        'zoomLevel': zoom,
    }

    url = "https://www.velib-metropole.fr/webapi/map/details"

    response = requests.get(url, params = get_params)
    if (response.status_code != 200):
        raise Exception('GET_REQUEST_BAD_STATUS', response.status_code)
    
    json_string = response.text
    return json.loads(json_string)


'''
 - gpsTopLatitude=48.993755859210625,
 - gpsTopLongitude=2.8059831283112374,
 - gpsBotLatitude=48.68950533278223,
 - gpsBotLongitude=1.884475865134607,
'''
def getAllStations():
    return searchStations(
        48.993755859210625,
        2.8059831283112374,
        48.68950533278223,
        1.884475865134607,
        11
    )